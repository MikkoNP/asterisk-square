﻿using System;

namespace TestApp5v
{
    class Program
    {
        static void Main(string[] args)
        {
            


            int size;
            bool numPos = false;
            
            //request and checking of correct input
            do
            {
                Console.WriteLine("a whole number, no negatives please");
                //someone told me that cool kids use TryParse()
                while (!int.TryParse(Console.ReadLine(), out size))
                {
                    Console.WriteLine("a whole number, no negatives please");
                }
                if (size > 0)
                {
                    numPos = true;
                } 
            } while (!numPos);

            
            //Ignore this. just some testing of basic try catch stuff to make sure I remember something about it
            /*
            string sizeStr;
            do
            {
                sizeStr = Console.ReadLine();
                try
                {
                    size = int.Parse(sizeStr);
                }
                catch
                {
                    Console.WriteLine("try again");
                }
                if (size > 0)
                {
                    numPos = true;
                }
            } while (!numPos);
            */
            

            //top row            
            for (int a = 0; a < size; a++)
            {
                //added spaces behind all * symbols to make the output look better.
                Console.Write("* ");
            }

            //mid rows
            Console.Write("\n");
            for (int a = 0; a < size-2; a++)
            {
                for (int b = 0; b < size; b++)
                {
                    if (b == 0 || b == size - 1)
                    {
                        Console.Write("* ");
                    }
                    else
                    {
                        //inner circle. it's not very pretty but it works...
                        if (size >= 5)
                        {
                            if ((a > 0  && a < size-3) && (b == 2 || b == size-3 ))
                            {
                                Console.Write("* ");

                            } else if ((a == 1 || a == size - 4) && (b > 2 && b < size-3))
                            {
                                Console.Write("* ");
                            }else
                                Console.Write("  ");
                        }
                        else 
                            Console.Write("  ");
                    }
                }
                Console.Write("\n");
            }

            //last row
            for (int a = 0; a < size && size > 1; a++)
            {
                Console.Write("* ");
            }
        }
    }
}
